CMAKE_MINIMUM_REQUIRED(VERSION 3.0.0)

set(exe_name "my_app")

file(GLOB my_src ${CMAKE_CURRENT_SOURCE_DIR}/src/*.*)
file(GLOB test_src ${CMAKE_CURRENT_SOURCE_DIR}/test/*.*)
list(APPEND test_src ${my_src})
list(REMOVE_ITEM test_src "${CMAKE_CURRENT_SOURCE_DIR}/src/main.cpp")
include_directories("${CMAKE_CURRENT_SOURCE_DIR}/src")

add_executable(${exe_name} ${my_src})
add_executable(${exe_name}_test ${test_src})

add_dependencies(${exe_name} my_lib)
add_dependencies(${exe_name}_test my_lib)


set_target_properties( ${exe_name}_test
    PROPERTIES
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/test"
)