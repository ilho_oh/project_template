CMAKE_MINIMUM_REQUIRED(VERSION 3.0.0)

## libraries for all executables
add_subdirectory(my_lib)
include_directories(my_lib/src)
link_libraries(my_lib)

## executables
add_subdirectory(my_app)
